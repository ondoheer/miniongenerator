from flask import Blueprint, render_template

front = Blueprint('index', __name__)

@front.route('/')
@front.route('/index')
def index():
    return render_template('index.html')