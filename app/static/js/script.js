$(document).ready(function(){
    $('.profession').hide();

    $('#lvlRange').on('chamnge', function(){
        console.log('change');
        $(this).val($('#lvlRange').val());
    });

    $('#race').on('change', function(){
        $('.profession').hide();
        switch($('#race').val()){
            case 'orc':
            $('.warrior').show();
            $('.ranger').show();
            $('.spellCaster').show();
            $('.barbarian').show();
            break;

            case 'goblin':
            $('.warrior').show();
            $('.ranger').show();
            $('.barbarian').show();
            break;

            case 'halfOrc':
            $('.warrior').show();
            $('.ranger').show();
            $('.spellCaster').show();
            $('.barbarian').show();
            $('.rogue').show();
            $('thief').show()
            break;

            case 'snaga':
            $('.warrior').show();
            $('.ranger').show();
            $('.thief').show();
            break;

            case 'uruk':
            $('.warrior').show();
            $('.ranger').show();
            $('.barbarian').show();
            $('.spellCaster').show();
            break;

            case 'caveTroll':
            $('.warrior').show();
            
            break;
        }
    });


    $('.execute').on('click', function(){
            
            var getProfessions = function(){
                var list = [];
                $('.profession').each(function(){
                    
                    if( $(this).prop('checked')){
                        list.push($(this).val());
                    }
                });
                return list;
            }
            
            
            
            var race = $('#race').val();
            var lvl = $('#lvlNumber').val();
            var number = $('#quantity').val();
            var profession = getProfessions();
            var minion = {
                "race": race,
                "profession": JSON.stringify(profession),
                "lvl": lvl,
                "number": number

            };
            console.log(minion);
            $.getJSON('/minions/generate', minion, function(minionReceived){
                console.log(minionReceived);
                for (var i = 0; i < minion.number; i++){
                    var min = minionReceived[i];
                    $("#container").append('<div class="minion" style="border: 2px solid #0e0e0e; float:left; width:300px; margin: 10px; height: 320px; overflow:auto; "><ul><li>Level: ' + min.lvl +
                    '</li><li>Profession: ' + min.profession + '</li>'+
                        '<li>Race: ' + min.race + '</li>'+
                        '<li>hits: ' + min.values.hits + '</li>'+
                        '<li>BO1:' + min.values.bo1 + '</li>'+
                        '<li>BO2:' + min.values.bo2 + '</li>'+
                        '<li>BD: ' + min.values.bd + '</li>'+
                        '<li>initiative: ' + min.values.ini + '</li>'+
                        '<li>perception: ' + min.values.perception + '</li>'+
                        '<li>RR Stamina: ' + min.values.rr_stam + '</li>'+
                        '<li>RR Will: ' + min.values.rr_will + '</li>'+
                        '<li>RR Will: ' + min.values.rr_will + '</li>'+
                        '<li>Talents: ' + min.talents + '</li>'+
                        '</ul></div>'

                    );
                }

            });

        });
});


