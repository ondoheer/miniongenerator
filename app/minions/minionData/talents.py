from random import choice

class Talents():

    def stat_bonus_minor(self, Character):

        stat = choice(Character.setProfessionStats())
        Character.stats[stat] += 1
        return Character

    def stat_bonus_lesser(self, Character):

        stat = choice(Character.setProfessionStats())
        Character.stats[stat] += 3
        return Character

    def stat_bonus_mayor(self, Character):

        stat = choice(Character.setProfessionStats())
        Character.stats[stat] += 5
        return Character

    def stat_bonus_greater(self, Character):

        stat = choice(Character.setProfessionStats())
        Character.stats[stat] += 8
        return Character

    def rr_stam_minor(self, Character):

        Character.values['rr_stam'] += 10
        return Character

    def rr_stam_mayor(self, Character):

        Character.values['rr_stam'] += 20
        return Character

    def rr_will_minor(self, Character):

        Character.values['rr_will'] += 10
        return Character

    def rr_will_mayor(self, Character):

        Character.values['rr_will'] += 20
        return Character

    def lightning_reflexes(self, Character):

        Character.values['ini'] += 5
        return Character

    def enhance_senses(self, Character):

        Character.values['perception'] += 5
        return Character

    def toughness(selfself, Character):

        Character.values['hits'] += 10
        return Character




