orc = {
	'stats' :{
            'co': 3,
            'sd': 0,
            'st': 6,
            'ag': 0,
            'qu': 0,
            'in': 0,
            're': 0,
            'pr': 0
    },
    'profession': {
        'available': ['warrior','ranger','spellCaster','barbarian'],
    },
    'values':{
        'hits': 40,
        'bo1': 15,
        'bo2': 10,
        'bd': 0,
        'rr_stam': 35,
        'rr_will': 10,
        'ini': 0,
        'perception': 0,
        'primary_skills': 15,
        'secondary_skills': 10,        
    },
}

goblin = {
	'stats' :{
            'co': 3,
            'sd': 0,
            'st': 3,
            'ag': 3,
            'qu': 0,
            'in': 0,
            're': 0,
            'pr': 0
    },
    'profession': {
        'available': ['warrior','ranger','barbarian'],
    },
    'values':{
        'hits': 30,
        'bo1': 15,
        'bo2': 10,
        'bd': 0,
        'rr_stam': 25,
        'rr_will': 10,
        'ini': 0,
        'perception': 0,
        'primary_skills': 15,
        'secondary_skills': 10,        
    },
}

halfOrc = {
	'stats' :{
            'co': 2,
            'sd': 2,
            'st': 2,
            'ag': 2,
            'qu': 0,
            'in': 0,
            're': 0,
            'pr': 0
    },
    'profession': {
        'available': ['warrior','ranger','barbarian', 'spellCaster','rogue','thief'],
    },
    'values':{
        'hits': 30,
        'bo1': 15,
        'bo2': 10,
        'bd': 0,
        'rr_stam': 20,
        'rr_will': 20,
        'ini': 0,
        'perception': 0,
        'primary_skills': 15,
        'secondary_skills': 10,        
    },
}

snaga = {
	'stats' :{
            'co': 2,
            'sd': 0,
            'st': 1,
            'ag': 2,
            'qu': 1,
            'in': 3,
            're': 0,
            'pr': 0
    },
    'profession': {
        'available': ['warrior','ranger','thief'],
    },
    'values':{
        'hits': 30,
        'bo1': 15,
        'bo2': 10,
        'bd': 0,
        'rr_stam': 25,
        'rr_will': 10,
        'ini': 0,
        'perception': 0,
        'primary_skills': 15,
        'secondary_skills': 10,        
    },
}

uruk = {
	'stats' :{
            'co': 4,
            'sd': 2,
            'st': 6,
            'ag': 2,
            'qu': 0,
            'in': 0,
            're': 0,
            'pr': 0
    },
    'profession': {
        'available': ['warrior','ranger','spellCaster','barbarian'],
    },
    'values':{
        'hits': 30,
        'bo1': 15,
        'bo2': 10,
        'bd': 0,
        'rr_stam': 25,
        'rr_will': 10,
        'ini': 0,
        'perception': 0,
        'primary_skills': 15,
        'secondary_skills': 10,        
    },
}

caveTroll = {
	'stats' :{
            'co': 12,
            'sd': 2,
            'st': 14,
            'ag': 1,
            'qu': 0,
            'in': 0,
            're': 0,
            'pr': 0
    },
    'profession': {
        'available': ['barbarian'],
    },
    'values':{
        'hits': 80,
        'bo1': 25,
        'bo2': 15,
        'bd': 0,
        'rr_stam': 65,
        'rr_will': 10,
        'ini': 0,
        'perception': 0,
        'primary_skills': 15,
        'secondary_skills': 10,        
    },
}