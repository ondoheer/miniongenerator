import unittest
import minionConstructor




class TestMinion(unittest.TestCase):


    def setUp(self):
        self.profession = 'warrior'
        self.race = 'orc'
        self.lvl = 0
        self.stats = {
            'co': 46,
            'sd': 46,
            'st': 46,
            'ag': 46,
            'qu': 46,
            'in': 46,
            're': 46
        }
        self.statsBonuses = {
            'co': 0,
            'sd': 0,
            'st': 0,
            'ag': 0,
            'qu': 0,
            'in': 0,
            're': 0
        }
        self.talents = ['superman']
        self.values = {
            'bo1': 0,
            'bd': 0
        }
        self.skills = {
            'primary': 0,
            'secondary': 0
        }



    def tearDown(self):
        """nothing yet"""



    def test_create(self):
        """asegura que todos los kwargs se esten procesando correctamente"""
        orc = minionConstructor.Minion(
            'orc',
            skills=self.skills,
            talents = self.talents,
            values = self.values,
            statBonuses = self.statsBonuses,
            stats = self.stats,
            profession = self.profession,
            lvl = self.lvl
        )


        assert orc.skills['primary'] == 0
        assert orc.talents is self.talents
        assert orc.values['bo1'] == 0
        assert orc.statBonuses['co'] == 0
        assert orc.stats is self.stats

    def test_set_profession_stats(self):
        """asegura que se asignen bien los stats segun la profesion"""
        orc = minionConstructor.Minion('orc', profession='warrior')
        assert orc.setProfessionStats() == ['st', 'ag', 'co', 'sd']
        orc = minionConstructor.Minion('orc', profession='ranger')
        assert orc.setProfessionStats() == ['sd', 'in', 'co', 'ag']
        orc = minionConstructor.Minion('orc', profession='barbarian')
        assert orc.setProfessionStats() == ['sd', 'st', 'co', 'ag']
        orc = minionConstructor.Minion('orc', profession='thief')
        assert orc.setProfessionStats() == ['in', 'ag', 'sd', 'pr']
        orc = minionConstructor.Minion('orc', profession='spellCaster')
        assert orc.setProfessionStats() == ['re', 'sd', 'pr', 'in']
        orc = minionConstructor.Minion('orc', profession='rogue')
        assert orc.setProfessionStats() == ['ag', 'qu', 'in', 'pr']
        orc = minionConstructor.Minion('orc')
        assert orc.setProfessionStats() == []

    def test_get_stat_bonus(self):
        """ checks if stat bonuses are assigned properly"""
        orc = minionConstructor.Minion('orc')
        assert orc.getStatBonus(48) == 0
        assert orc.getStatBonus(96) == 10
        assert orc.getStatBonus(46) == 0
        assert orc.getStatBonus(67) == 4
        assert orc.getStatBonus(83) == 7
        assert orc.getStatBonus(92) == 9

    def test_level_up_stat(self):
        """tests if stats are leveled up properly"""
        orc = minionConstructor.Minion('orc')
        orc.lvl = 10
        orc.profession = 'warrior'
        orc.levelUpStats()
        assert orc.statBonuses['st'] > 6
        assert orc.statBonuses['qu'] >= 1
        orc2 = minionConstructor.Minion('orc')
        orc2.lvl = 2
        orc2.profession = 'warrior'
        orc2.levelUpStats()
        assert orc2.statBonuses['st'] > 0
        assert orc2.statBonuses['qu'] >= 0
        orc3 = minionConstructor.Minion('orc')
        orc3.lvl = 2
        orc3.profession = 'spellCaster'
        orc3.levelUpStats()
        assert orc3.statBonuses['re'] >= 1
        assert orc3.statBonuses['st']  >= 0

        orc4 = minionConstructor.Minion('orc')
        orc4.lvl = 10
        orc4.profession = 'ranger'
        orc4.levelUpStats()
        assert orc4.statBonuses['sd'] >= 6
        assert orc4.statBonuses['st'] >= 2

    def test_level_up_skills(self):
        """tests if skills are being maximized according to the profession"""

        orc = minionConstructor.Minion('orc')
        orc.lvl = 2
        orc.profession = 'warrior'
        orc.levelUpStats()
        orc.levelUpSkills
        assert orc.skills['primary'] == 55
        assert orc.skills['secondary'] == 30
        orc2 = minionConstructor.Minion('orc')
        orc2.lvl = 6
        orc2.profession = 'barbarian'
        orc2.levelUpStats()
        orc2.levelUpSkills
        assert orc2.skills['primary'] == 86
        assert orc2.skills['secondary'] == 58

    def test_get_skills(self):
        """checks if the right skills are assigned to the right profession"""
        orc = minionConstructor.Minion('orc')
        orc.profession = 'warrior'
        assert orc.getSkills()['primary'] == [
                'perception',
                'rr_stam',
                'rr_will',
                'endurance',
                'armor',
                'brawling',
                'first_weapon',
                'second_weapon',
                'two_weapon'
                ]
        assert orc.getSkills()['secondary'] == [
                'lie_perception',
                'climbing',
                'swimming',
                'third_weapon',
                'jumping',
                'leadership'
        ]

    def test_get_talents(self):
        """selects the general talents and the profession talents"""
        orc = minionConstructor.Minion('orc')
        orc.profession = 'warrior'
        assert orc.getTalents == [
            'stat_bonus_minor',
            'stat_bonus_lesser',
            'stat_bonus_mayor',
            'stat_bonus_greater',
            'lie_perception',
            'rr_stam_minor',
            'rr_stam_mayor',
            'rr_will_minor',
            'rr_will_mayor',
            'shield_training',
            'quick_draw',
            'bane',
            'battle_cry',
            'combat_awareness',
            'combat_reflexes',
            'general_weapon_master',
            'hardy',
            'lightning_reflexes',
            'tolerance',
            'toughness',
            'survival_instinct'
        ]

    def test_set_talents(self):
        """elects a talent from the talent pool for that class"""
        orc = minionConstructor.Minion('orc')
        orc.profession = 'warrior'


    def test_calculate_values(self):
        orc = minionConstructor.Minion('orc')
        orc.profession = 'warrior'
        orc.talents = ['stat_bonus_lesser']
        orc.calculateValues()
        assert 1 in orc.statBonuses.values()

        orc2 = minionConstructor.Minion('orc')
        orc2.profession = 'rogue'
        orc2.talents = ['stat_bonus_greater']
        orc2.calculateValues()
        assert 8 in orc2.statBonuses.values()

        orc3 = minionConstructor.Minion('orc')
        orc3.talents = ['rr_stam_minor']
        orc3.calculateValues()
        assert orc3.values['rr_stam'] == 10

        orc4 = minionConstructor.Minion('orc')
        orc4.talents = ['rr_will_minor']
        orc4.calculateValues()
        assert orc4.values['rr_will'] == 10

        orc5 = minionConstructor.Minion('orc')
        orc5.talents = ['lightning_reflexes']
        orc5.calculateValues()
        assert orc5.values['ini'] == 5

        orc6 = minionConstructor.Minion('orc')
        orc6.talents = ['enhance_senses']
        orc6.calculateValues()
        assert orc6.values['perception'] == 10






if __name__ == '__main__':

   orc = minionConstructor.Minion('orc')
   orc.profession = 'barbarian'
   orc.setProfessionStats()
   orc.lvl = 6
   orc.levelUpStats()
   orc.getSkills()
   orc.levelUpSkills
   talents = orc.getTalents
   orc.setTalents(talents)
   orc.calculateValues()
   print 'stats'
   print orc.stats
   print 'statBonuses'
   print orc.statBonuses
   print 'values'
   print orc.values
   print 'talents'
   print orc.talents
   print 'skills'
   print orc.skills

   unittest.main()