minion = {
    'race' :
        {
        'level': 0,
        'stats' :{
            'co': 0,
            'sd': 0,
            'st': 0,
            'ag': 0,
            'qu': 0,
            'in': 0,
            're': 0
        },
        'profession': {
            'available': [],
            'selected': ''
        },
        'talents':[],
        'flaws': [],
        'values':{
            'hits': 0,
            'bo1': 0,
            'bo2': 0,
            'bd': 0,
            'rr_stam': 0,
            'rr_will': 0,
            'ini': 0,
            'perception': 0,
            'primary_skills': 0,
            'secondary_skills': 0,
        },
        'skills':{
            'primary':[],
            'secondary': []
        }


    },
}
