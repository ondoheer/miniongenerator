from __future__ import division
from random import randint, sample, randrange, choice
from collections import OrderedDict
import races

avaliableRaces = {
    'orc': races.orc,
    'goblin': races.goblin,
    'halfOrc': races.halfOrc,
    'snaga': races.snaga,
    'uruk': races.uruk,
    'caveTroll': races.caveTroll
}

class Minion(object):

    """genera los stats de un minion"""

    def __init__(self, race, **kwargs):
        """
        accepts a race and the different keys in the json that compose a character
        race -> string
        lvl -> int
        stats -> dict
        profession -> string
        values -> dict
        skills -> dict
        """
        self.race = race
        self.lvl = 0
        if 'lvl' in kwargs:
            self.lvl = kwargs['lvl']
        self.stats = {
            'co': randrange(32,51),
            'sd': randrange(32,51),
            'st': randrange(32,51),
            'ag': randrange(32,51),
            'qu': randrange(32,51),
            'in': randrange(32,51),
            're': randrange(32,51),
            'pr': randrange(32,51)
        }
        self.raceStats = avaliableRaces[race]['stats']
        if 'stats' in kwargs:
            self.stats = kwargs['stats']
        self.statBonuses = { 
            'co': 0,
            'sd': 0,
            'st': 0,
            'ag': 0,
            'qu': 0,
            'in': 0,
            're': 0,
            'pr': 0
        }
        if 'statBonuses' in kwargs:
            for stat, value in kwargs['statBonuses'].iteritems():
                self.statBonuses[stat] = value
        
        if 'profession' in kwargs:
            self.profession = kwargs['profession']
        self.talents = []
        if 'talents' in kwargs:
            self.talents = kwargs['talents']
        self.values = {
            'hits': self.statBonuses['co'] + self.statBonuses['sd'],
            'bo1': self.statBonuses['st'] + self.statBonuses['ag'],
            'bo2': self.statBonuses['st'] + self.statBonuses['ag'],
            'bd': self.statBonuses['qu'] * 2,
            'rr_stam': self.statBonuses['co'] + self.statBonuses['sd'],
            'rr_will': self.statBonuses['sd'] * 2,
            'ini': self.statBonuses['qu'] + self.statBonuses['in'],
            'perception': self.statBonuses['re'] + self.statBonuses['in']

        }
        if 'values' in kwargs:
            self.values.update(kwargs['values'])
        self.skills = {'primary': 0, 'secondary': 0, 'other': 0}
        if 'skills' in kwargs:
            self.skills.update(kwargs['skills'])

    def setProfessionStats(self):
        '''returns an array with the stats to level up for each profession
        if no profession was set it returns an empty array'''
        professions = dict(
            warrior=['st', 'ag', 'co', 'sd', 'qu'],
            ranger=['sd', 'in', 'co', 'ag', 'qu'],
            barbarian=['sd', 'st', 'co', 'ag', 'qu'],
            thief=['in', 'ag', 'sd', 'pr', 'qu'],
            spellCaster=['re', 'sd', 'pr', 'in'],
            rogue=['ag', 'qu', 'in', 'pr', 'st'])
        if self.profession in professions:
            return professions[self.profession]
        return []

    @staticmethod
    def getStatBonus(stat):
        """
        recibe un valor de stat y le devuelve un bonus

        :param stat: int
        :return: int
        """
        values = {
            46: 0,
            51: 1,
            56: 2,
            61: 3,
            66: 4,
            71: 5,
            76: 6,
            81: 7,
            86: 8,
            91: 9,
            96: 10,
            101: 11,
            102: 12,
            103: 13,
            104: 14,
            105: 15
        }
        values = OrderedDict(sorted(values.items(), key=lambda t: t[0]))
        for key, bonus in values.iteritems():
            if stat < 51:
                return 0
            elif stat < key:
                return bonus - 1

    def levelUpStats(self):
        """
        genera temp stats subidos en base a los stats de un diccionario de profesion
        :type stats: dict
        """
        stats = self.setProfessionStats()

        for lvl in xrange(1, self.lvl + 1):
            for stat in self.stats:
                if stat in stats and randrange(5) < 5:
                    if self.stats[stat] <= 86:
                        self.stats[stat] += 4
                    elif self.stats[stat] <= 91:
                        self.stats[stat] += 2
                    elif self.stats[stat] <= 96:
                        self.stats[stat] += 1
                elif stat not in stats and randrange(5) < 4:
                    if self.stats[stat] <= 86:
                        self.stats[stat] += 2
                    elif self.stats[stat] <= 91:
                        self.stats[stat] += 1
                    elif self.stats[stat] <= 96:
                        self.stats[stat] += 0

        for stat in self.stats:
            self.statBonuses[stat] = self.getStatBonus(self.stats[stat]) + self.raceStats[stat]

    @property
    def levelUpSkills(self):
        """
        genera el valor para los skills primarios y secundarios

        :return: dict
        """

        ranks_primary = (self.lvl * 3) + 3
        ranks_secondary = self.lvl * 2 + 2
        ranks_other = self.lvl * 1

        def getRankValue(ranks):
            if 0 < ranks < 11:
                value = ranks * 5
            elif 10 < ranks < 21:
                value = 50 + ((ranks - 10) * 2)
            else:
                value = 70 + (ranks - 20)
            return value

        def lvlSpecialBonus(lvl):
            value = 0
            if self.profession in ['rogue', 'barbarian']:
                if 0 < lvl < 3:
                    value += 5
                elif 3 <= lvl < 6:
                    value += 10
                elif 6 <= value < 9:
                    value += 15
                else:
                    value += 15
                return value
            else:
                if 0 <= lvl < 5:
                    value += 10
                elif 5 <= value < 10:
                    value += 20
                elif 10 <= value < 15:
                    value += 30
                else:
                    value += 30
                return value

        self.skills[
            'primary'] += (getRankValue(ranks_primary) + lvlSpecialBonus(self.lvl))
        self.skills['secondary'] += getRankValue(ranks_secondary)
        self.skills['other'] += getRankValue(ranks_other)

    def getSkills(self):
        """

        :return: dictionary with the primary and secondary skills for that class
        """
        skills = dict(
            general=dict(primary=[
                'perception',
                'rr_stam',
                'rr_will',
                'endurance'
            ],
                secondary=[
                'lie_perception',
                'climbing',
                'swimming'
            ]),
            warrior=dict(
                primary=[
                    'armor',
                    'brawling',
                    'first_weapon',
                    'second_weapon',
                    'two_weapon'
                ],
                secondary=[
                    'third_weapon',
                    'jumping',
                    'leadership',
                ]
            ),
            ranger=dict(
                primary=[
                    'first_weapon',
                    'survival',
                    'stalking',
                    'second_weapon'
                ],
                secondary=[
                    'herbcraft',
                    'traps_craft',
                    'ambush_sniping',
                ]
            ),
            barbarian=dict(
                primary=[
                    'brawling',
                    'first_weapon',
                    'second_weapon',
                    'two_weapon',
                    'frenzy'
                ],
                secondary=[
                    'armor',
                    'third_weapon',
                    'jumping',
                    'leadership',
                    'foraging'
                ]
            ),
            thief=dict(
                primary=[
                    'first_weapon',
                    'poisoning',
                    'stalking',
                    'pick_pockets'
                ],
                secondary=[
                    'duping',
                    'jumping',
                    'acrobatics',
                    'ambush_sniping'
                ]
            ),
            spellCaster=dict(
                primary=[
                    'spellcasting',
                    'attunement',
                    'divination',
                    'directed_Spells'
                ],
                secondary=[
                    'runes',
                    'diplomacy',
                    'lores',
                ]
            ),
            rogue=dict(
                primary=[
                    'first_weapon',
                    'second_weapon',
                    'stalking',
                    'ambush_sniping'

                ],
                secondary=[
                    'two_weapon',
                    'diplomacy',
                    'trading'
                ]
            )
        )
        joined = skills['general']
        if self.profession in skills:
            for skill in skills[self.profession]['primary']:
                joined['primary'].append(skill)
            for skill in skills[self.profession]['secondary']:
                joined['secondary'].append(skill)
        return joined

    @property
    def getTalents(self):
        """
        genera arrays de talentos para utilizar

        :return:
        """
        talents = dict(
            general=[
                'stat_bonus_minor',
                'stat_bonus_lesser',
                'stat_bonus_mayor',
                'stat_bonus_greater',
                'lie_perception',
                'rr_stam_minor',
                'rr_stam_mayor',
                'rr_will_minor',
                'rr_will_mayor'
            ], warrior=[
                'shield_training',
                'quick_draw',
                'bane',
                'battle_cry',
                'combat_awareness',
                'combat_reflexes',
                'general_weapon_master',
                'hardy',
                'lightning_reflexes',
                'tolerance',
                'toughness',
                'survival_instinct'

            ], ranger=[
                'walk_without_trace',
                'tireless',
                'speed_loader',
                'outdoorsman',
                'night_training',
                'lightning_reflexes',
                'hardmarch',
                'enhance_senses',

            ], barbarian=[
                'quick_draw',
                'tolerance',
                'toughness',
                'bane',
                'battle_cry',
                'hardy',
                'survival_instinct'
            ], thief=[
                'assassin_training',
                'quick_draw',
                'extremely_nimble',
                'subtle',
                'manual_deftness',
                'survival_instinct'
            ], spellCaster=[
                'destiny_sense',
                'fluent',
                'incorruptible',
                'mana_reading',
                'mental_resolve',
                'photographic_memory',
                'sense_magic'
            ], rogue=[
                'assassin_training',
                'quick_draw',
                'extremely_nimble',
                'treacherous_blow',
                'survival_instinct',
            ])
        if self.profession in talents:
            joined = talents['general'] + talents[self.profession]
            return joined
        return talents['general']

    def setTalents(self, talentsAvailable):
        """ sets the talents for the char"""

        self.talents = sample(talentsAvailable, randint(1, 3))

    def stat_bonus_lesser(self):
        stat = choice(self.setProfessionStats())
        self.statBonuses[stat] += 1

    def stat_bonus_minor(self):
        stat = choice(self.setProfessionStats())
        self.statBonuses[stat] += 3

    def stat_bonus_mayor(self):
        stat = choice(self.setProfessionStats())
        self.statBonuses[stat] += 5

    def stat_bonus_greater(self):
        stat = choice(self.setProfessionStats())
        self.statBonuses[stat] += 8

    def rr_stam_minor(self):
        self.values['rr_stam'] += 10

    def rr_stam_mayor(self):
        self.values['rr_stam'] += 20

    def rr_will_minor(self):
        self.values['rr_will'] += 10

    def rr_will_mayor(self):
        self.values['rr_will'] += 20

    def lightning_reflexes(self):
        self.values['ini'] += 5

    def enhance_senses(self):
        self.values['perception'] += 10

    def toughness(self):
        self.values['hits'] += 10

    def survival_instinct(self):
        self.values['bd'] += 10

    def calculateValues(self):
        """
        it will generate the values for all the skills needed for the front end
        recalculate BO and BD according to professions
        :param profession: string
        :return:
        """
        bonusTalents = {  # will be added to the character
            'stat_bonus_lesser': self.stat_bonus_lesser,
            'stat_bonus_minor': self.stat_bonus_minor,
            'stat_bonus_mayor': self.stat_bonus_mayor,
            'stat_bonus_greater': self.stat_bonus_greater,
            'rr_stam_minor': self.rr_stam_minor,
            'rr_stam_mayor': self.rr_stam_mayor,
            'rr_will_minor': self.rr_will_minor,
            'rr_will_mayor': self.rr_will_mayor,
            'lightning_reflexes': self.lightning_reflexes,
            'enhance_senses': self.enhance_senses,
            'toughness': self.toughness,
            'survival_instinct': self.survival_instinct,

        }
        stringTalents = {  # will be returned as strings for the master to use
            'lie_perception': {20},
            'outdoorsman': {15},
            'subtle': {10},
            'manual_deftness': {10},
            'extremely_nimble': {10}
        }

        def calculateBOandHits():
            if self.profession is not 'spellCaster':
                self.values['bo1'] += self.skills['primary']
                if self.profession in ['warrior', 'barbarian']:
                    self.values['bo2'] += self.skills['primary']
                    self.values['hits'] += self.skills['primary']
                elif self.profession in ['rogue', 'ranger', 'thief']:
                    self.values['bo2'] += self.skills['secondary']
                    self.values['hits'] += self.skills['secondary']
                elif self.profession in ['spellCaster']:
                    self.values['hits'] += self.skills['secondary']

        def calculateResistances():
            if self.profession in ['warrior', 'barbarian']:
                self.values['rr_stam'] += self.skills['primary']
                self.values['rr_will'] += self.skills['secondary']
            elif self.profession in ['rogue', 'ranger', 'thief']:
                self.values['rr_stam'] += self.skills['secondary']
                self.values['rr_will'] += self.skills['secondary']
            elif self.profession in ['spellCaster']:
                self.values['rr_will'] += self.skills['primary']
                self.values['rr_stam'] += self.skills['other']

        def calculatePerception():
            if self.profession in ['barbarian', 'thief', 'ranger', 'rogue']:
                self.values['perception'] += self.skills['primary']
            elif self.profession in ['warrior', 'spellCaster']:
                self.values['perception'] += self.skills['secondary']

        self.values = {
            'hits': self.statBonuses['co'] + self.statBonuses['sd'] + avaliableRaces[self.race]['values']['hits'],
            'bo1': self.statBonuses['st'] + self.statBonuses['ag'] + avaliableRaces[self.race]['values']['bo1'],
            'bo2': self.statBonuses['st'] + self.statBonuses['ag'] + avaliableRaces[self.race]['values']['bo2'],
            'bd': self.statBonuses['qu'] * 2 + avaliableRaces[self.race]['values']['bd'],
            'rr_stam': self.statBonuses['co'] + self.statBonuses['sd'] + avaliableRaces[self.race]['values']['rr_stam'],
            'rr_will': self.statBonuses['sd'] * 2 + avaliableRaces[self.race]['values']['rr_will'],
            'ini': self.statBonuses['qu'] + self.statBonuses['in'] + avaliableRaces[self.race]['values']['ini'],
            'perception': self.statBonuses['re'] + self.statBonuses['in'] + avaliableRaces[self.race]['values']['perception']
        }

        for talent in self.talents:
            if talent in bonusTalents.keys():
                # ejecuta la funcion desde su diccionario y actualiza el PJ
                bonusTalents[talent]()

        calculateBOandHits()
        calculateResistances()
        calculatePerception()

        def setRace(self):
            return

        return self
