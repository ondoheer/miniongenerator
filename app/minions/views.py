from flask import Blueprint, render_template, request, jsonify
from minionData import minionConstructor
from random import choice
import json

minions = Blueprint('minions', __name__, url_prefix='/minions')


@minions.route('/')
def index():
    return render_template('minions.html')


@minions.route('/generate', methods=['GET'])
def generate():
    race = request.args.get('race')
    profession = json.loads(request.args.get('profession'))
    lvl = int(request.args.get('lvl'))
    number = int(request.args.get('number'))

    minions = {}
    for number in xrange(number):
        chosenProfession = choice(profession)
        chosenLvl = choice(xrange(lvl-3, lvl+3))
        min = minionConstructor.Minion(
            race,
            profession=chosenProfession,
            lvl=chosenLvl
        )
        min.setProfessionStats()
        min.levelUpStats()
        min.getSkills()
        min.levelUpSkills
        min.setTalents(min.getTalents)
        min.calculateValues()
        minions.update({

            number: {
                'race': race,
                'lvl': min.lvl,
                'stats': min.stats,
                'statBonuses': min.statBonuses,
                'profession': min.profession,
                'talents': min.talents,
                'values': min.values,
                'skills': min.skills

            }

        })

    return jsonify(minions)
