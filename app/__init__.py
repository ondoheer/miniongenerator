from flask import Flask
from minions.views import minions
from front.views import front
from minions import minionData

app = Flask(__name__)
app.register_blueprint(minions)
app.register_blueprint(front)
